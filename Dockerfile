FROM openjdk:8-jdk

ARG jar_file

WORKDIR /app

VOLUME /tmp

ADD $jar_file /app/java-project-1.0-SNAPSHOT.jar

CMD [ "java", "-jar", "java-project-1.0-SNAPSHOT.jar 5100" ]
